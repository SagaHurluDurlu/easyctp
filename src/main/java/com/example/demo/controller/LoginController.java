package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class LoginController {
	
	@Autowired
	UserService userService;
	
	private Map<String,User> loggedUsers=new HashMap<>();

	@RequestMapping(value = "/login")
	public String login(Model model) {
		model.addAttribute("user",new User());
	return "/start/login.jsp";
	}

	@RequestMapping(value="/autentify")
	public String autentify(@ModelAttribute("user") User user,HttpSession session,Model model) {
			User userT=userService.login(user);
			if(userT!=null ) {
				user.setType(userT.getType());
				if(loggedUsers.containsKey("user")) {
					User userSes=(User)session.getAttribute("user");
					if(userSes.getUsername().equals(user.getUsername())) {
						model.addAttribute("User already logged in!");
					}
				}
				else {
					session.setAttribute("loguser", userT);

					if(userT.getType().equals(UserType.USER)) {

						return "start/login.jsp";
					}
					else if(userT.getType().equals(UserType.ADMIN)) {

						return "redirect:/secured/admin/adminHome.jsp";
					}

				}
			}
			model.addAttribute("errorMessage","Wrong data. Try again!");
			return "start/login.jsp";
	}
	
	@RequestMapping(value="/logout")
	public String logout(Model model,HttpSession session) {
		session.removeAttribute("user");
		session.invalidate();
		return "/login";

}



}