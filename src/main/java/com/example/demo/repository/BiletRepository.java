package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Bilet;

@Repository
public interface BiletRepository extends JpaRepository<Bilet,Long>{

	public void deleteById(Integer id);

	public Bilet findById(Integer id);
}