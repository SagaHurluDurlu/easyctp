package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Abonament;

@Repository
public interface AbonamentRepository extends JpaRepository<Abonament,Long>{

	public void deleteById(Integer id);

	public Abonament findById(Integer id);
}
