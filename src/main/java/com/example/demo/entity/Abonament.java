package com.example.demo.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.Enum.AbonamentType;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Abonament")
public class Abonament extends BaseEntity {
	@Enumerated(EnumType.STRING)
	private AbonamentType type;
	private Long price;
	private int duration;
	private Date start_date;
	private Date stop_date;

	@OneToMany(targetEntity=AbonamentUser.class, mappedBy="abonament", fetch=FetchType.EAGER)
	protected List<AbonamentUser> abonament;
	@OneToMany(targetEntity=Lines.class, mappedBy="AbonamentLines", fetch=FetchType.EAGER)
	protected Set<Lines> AbonamentLines;
	
	private int numberOfTravelers;
	
	
	
	public Abonament(AbonamentType type, Long price, int duration, Date start_date, Date stop_date,
			List<AbonamentUser> abonament, Set<Lines> abonamentLines, int numberOfTravelers) {
		super();
		this.type = type;
		this.price = price;
		this.duration = duration;
		this.start_date = start_date;
		this.stop_date = stop_date;
		this.abonament = abonament;
		AbonamentLines = abonamentLines;
		this.numberOfTravelers = numberOfTravelers;
	}
	
	public Abonament(Abonament ab) {
		super();
		this.type = ab.getType();
		this.price = ab.getPrice();
		this.duration = ab.getDuration();
		this.start_date = ab.getStart_date();
		this.stop_date = ab.getStop_date();
		this.abonament = ab.getAbonament();
		AbonamentLines = ab.getAbonamentLines();
		this.numberOfTravelers = ab.getNumberOfTravelers();
	}
	
	public Abonament() {
	}
	public AbonamentType getType() {
		return type;
	}
	public void setType(AbonamentType type) {
		this.type = type;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getStop_date() {
		return stop_date;
	}
	public void setStop_date(Date stop_date) {
		this.stop_date = stop_date;
	}
	public List<AbonamentUser> getAbonament() {
		return abonament;
	}
	public void setAbonament(List<AbonamentUser> abonament) {
		this.abonament = abonament;
	}
	public Set<Lines> getAbonamentLines() {
		return AbonamentLines;
	}
	public void setAbonamentLines(Set<Lines> abonamentLines) {
		AbonamentLines = abonamentLines;
	}
	public int getNumberOfTravelers() {
		return numberOfTravelers;
	}
	public void setNumberOfTravelers(int numberOfTravelers) {
		this.numberOfTravelers = numberOfTravelers;
	} 
	
	
}
