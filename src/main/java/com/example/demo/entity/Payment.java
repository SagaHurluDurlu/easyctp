package com.example.demo.entity;

import java.text.DateFormat;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Payment")
public class Payment extends BaseEntity{

	private Long amount;
	private DateFormat dateOfPay;
	@ManyToOne
	private CardBancar cardBancar;
	@ManyToOne
	private User ownerPay;
	
	
	public Payment(Long amount, DateFormat dateOfPay, CardBancar cardBancar, User user) {
		super();
		this.amount = amount;
		this.dateOfPay = dateOfPay;
		this.cardBancar = cardBancar;
		this.ownerPay = user;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public DateFormat getDateOfPay() {
		return dateOfPay;
	}
	public void setDateOfPay(DateFormat dateOfPay) {
		this.dateOfPay = dateOfPay;
	}
	public CardBancar getCardBancar() {
		return cardBancar;
	}
	public void setCardBancar(CardBancar cardBancar) {
		this.cardBancar = cardBancar;
	}
	public User getUser() {
		return ownerPay;
	}
	public void setUser(User user) {
		this.ownerPay = user;
	}
}
