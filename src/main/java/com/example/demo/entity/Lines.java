package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.Enum.LineType;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Lines")
public class Lines extends BaseEntity{
	@ManyToOne
	protected AbonamentUser AbonamentLines;
	
	@Enumerated(EnumType.STRING)
	private LineType line;

	
	
	public Lines(AbonamentUser abonamentLines, LineType line) {
		super();
		AbonamentLines = abonamentLines;
		this.line = line;
	}

	public Lines() {
	}

	public LineType getLine() {
		return line;
	}

	public void setLine(LineType line) {
		this.line = line;
	}

	public AbonamentUser getAbonamentLines() {
		return AbonamentLines;
	}

	public void setAbonamentLines(AbonamentUser abonamentLines) {
		AbonamentLines = abonamentLines;
	}
}
