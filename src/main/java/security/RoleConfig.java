package security;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class RoleConfig {

  @Bean
  public FilterRegistrationBean<RoleFilter> roleFilter() {
    FilterRegistrationBean<RoleFilter> registration = new FilterRegistrationBean<>();
    registration.setFilter(new RoleFilter());
    registration.addUrlPatterns("/secured/admin/*");
    return registration;
  }
}
