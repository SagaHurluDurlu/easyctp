package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.AbonamentUser;

@Repository
public interface AbonamentUserRepository  extends JpaRepository<AbonamentUser,Long>{

	public void deleteById(Integer id);

	public AbonamentUser findById(Integer id);


}
