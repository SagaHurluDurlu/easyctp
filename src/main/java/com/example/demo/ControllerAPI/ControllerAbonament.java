package com.example.demo.ControllerAPI;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Enum.AbonamentType;
import com.example.demo.entity.Abonament;
import com.example.demo.service.AbonamentService;
import com.example.demo.service.AbonamentUserService;
import com.example.demo.service.LinesService;
import com.example.demo.service.UserService;

@RestController
public class ControllerAbonament {

	@Autowired
	UserService userService;
	@Autowired
	AbonamentService abonamentService;
	@Autowired
	AbonamentUserService abonamentUserService;
	@Autowired
	LinesService linesService;
	
	
	@RequestMapping(value="/createAbonament", method = RequestMethod.POST)
	@Procedure(MediaType.APPLICATION_JSON)
	public ResponseEntity<?> createAb(@RequestBody Abonament abonament ,HttpServletRequest request,Model model,@RequestParam String abtype,
			@RequestParam(required=false,name="ltype1") String ltype1,@RequestParam(required=false,name="ltype2") String ltype2,
			@RequestParam Integer id) {
		model.addAttribute("abonamentType",Arrays.asList(AbonamentType.values()));
		Abonament ab = new Abonament();
		ab.setDuration(abonament.getDuration());
		abonamentService.createAbonament(ab,id,ltype1,ltype2,abtype,abonament.getStart_date());
		return new ResponseEntity <String> ("Create", HttpStatus.OK);
	}
}
