package com.example.demo.entity;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.example.demo.Enum.Status;
import com.example.demo.Enum.UserType;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="ApplicationUser")
public class User extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String fullName;
	private String adress;
	private String email;
	@Enumerated(EnumType.STRING)
	private UserType type;
	private String nrTelefon;
	@Enumerated(EnumType.STRING)
	private Status status;
	private String cod;
	
	@OneToMany(targetEntity=AbonamentUser.class, mappedBy="owner", fetch=FetchType.EAGER)
	protected List<AbonamentUser> abonament;
	
	@OneToMany(targetEntity=CardBancar.class, mappedBy="ownerCard", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	protected List<CardBancar> cardList;
	
	@OneToMany(targetEntity=Payment.class, mappedBy="ownerPay", fetch=FetchType.EAGER)
	protected Set<Payment> payments;
	
	@OneToMany(targetEntity=Bilet.class, mappedBy="ownerTiket", fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	protected Set<Bilet> bilet;
	
	 public User() {}

    public User(User users) {
    	this.username = users.getUsername();
        this.email = users.getEmail();
        this.adress = users.getAdress();
        this.type= users.getType();
        this.fullName =users.getFullName();
        this.password = users.getPassword();
        this.nrTelefon = users.getNrTelefon();
        this.status = users.getStatus();
        this.cardList = users.getCardList();
        this.cod=users.getCod();
    }
    
	public User(String username, String password, String fullName, String adress, String email, UserType type,
			String nrTelefon, Status status, List<AbonamentUser> abonament, List<CardBancar> cardList, Set<Payment> payments) {
		super();
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.adress = adress;
		this.email = email;
		this.type = type;
		this.nrTelefon = nrTelefon;
		this.status = status;
		this.abonament = abonament;
		this.cardList = cardList;
		this.payments = payments;
	}

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public UserType getType() {
		return type;
	}
	public void setType(UserType type) {
		this.type = type;
	}

	
	public String getUsername() {
		return username;
	}
	public void setUsername(String userame) {
		this.username = userame;
	}
	
	
	public List<CardBancar> getCardList() {
		return cardList;
	}

	public void setCardList(List<CardBancar> cardList) {
		this.cardList = cardList;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setAbonament(List<AbonamentUser> abonament) {
		this.abonament = abonament;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String passworld) {
		this.password = passworld;
	}
	

	public String getNrTelefon() {
		return nrTelefon;
	}

	public void setNrTelefon(String nrTelefon) {
		this.nrTelefon = nrTelefon;
	}

	public List<AbonamentUser> getAbonament() {
		return abonament;
	}
	public AbonamentUser getAbonamentByID(Integer id) {
		return abonament.get(id);
	}


	public void setAccounts(List<AbonamentUser> abonament) {
		this.abonament = abonament;
	}

	public Set<Payment> getPayments() {
		return payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}
	
}
