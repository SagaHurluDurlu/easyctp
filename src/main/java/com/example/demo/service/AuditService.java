package com.example.demo.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Audit;
import com.example.demo.repository.AuditRepository;

@Service
@Transactional
public class AuditService {
	private final AuditRepository auditRepository;

	public AuditService(AuditRepository auditRepository) {
		this.auditRepository = auditRepository;
	}
	public void save(Audit audit ) {
		auditRepository.save(audit);
	}
	

}
