package com.example.demo.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.example.demo.Enum.LineType;
import com.example.demo.entity.Lines;
import com.example.demo.repository.LinesRepository;

@Service
@Transactional
public class LinesService {
	private final LinesRepository linesRepository;

	private AbonamentUserService abUsService;
	
	public LinesService(LinesRepository linesRepository) {
		this.linesRepository = linesRepository;
	}
	
	public void save(Lines lines ) {
		linesRepository.save(lines);
	}
	public void save(Integer id,LineType lType ) {
		Lines line = new Lines();
		line.setLine(lType);
		line.setAbonamentLines(abUsService.findById(id));
		linesRepository.save(line);
	}

}
