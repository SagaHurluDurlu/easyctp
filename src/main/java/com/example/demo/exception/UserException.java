package com.example.demo.exception;

import java.util.HashMap;
import java.util.Map;

public class UserException extends Exception {
	private static final long serialVersionUID = 1L;
	Map<String, String> errors  = new HashMap<>();
	
	public UserException() {
		super();
	}
	
	public UserException(Map<String, String> err) {
		this.errors=err;
	}
	
	public void addException(String errorName, String error) {
		errors.put(errorName, error);
	}
	
	public Map<String, String> getErrors(){
		return errors;
	}
}