package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Station;

@Repository
public interface StationRepository extends JpaRepository<Station,Long>{

	public void deleteById(Integer id);

	public Station findById(Integer id);
}
