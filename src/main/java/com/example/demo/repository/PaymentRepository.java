package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Payment;

@Repository
public interface PaymentRepository extends JpaRepository <Payment,Long>{

	public void deleteById(Integer id);

	public Payment findById(Integer id);
}
