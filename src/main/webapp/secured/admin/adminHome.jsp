<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>

<meta charset="utf-8">

  <title>Easy CTP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/secured/admin/show-info">Home</a></li>
      	<li><a href="/secured/admin/register">New Registration</a></li>
       <li><a href="/secured/admin/show-user">List of users</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/logout" ><span class="glyphicon glyphicon-user"></span> Log out</a></li>
    </ul>
  </div>
  </nav>
</body>
</html>

  