<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="adminHome.jsp"></jsp:include>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bank Application</title>

</head>
<body>

<div class="container text-center">
				<h3>New Register</h3>
				<form class="form-horizontal" method="POST" action="save-user">
					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-7">
							<input type="text" required class="form-control" name="username"
								value="${newUser.username}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Full Name</label>
						<div class="col-md-7">
							<input type="text" required class="form-control" name="fullName"
								value="${newUser.fullName}" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Address</label>
						<div class="col-md-7">
							<input type="text" required class="form-control" name="adress"
								value="${newUser.adress}" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Email </label>
						<div class="col-md-3">
							<input type="text" required class="form-control" name="email"
								value="${newUser.email}" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Telefon </label>
						<div class="col-md-3">
							<input type="text" required class="form-control" name="nrTelefon"
								value="${newUser.nrTelefon}" />
						</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3">Status </label>
					<div class="col-md-3">
					<select name="status"class="form-control">
                        <c:forEach items="${Status}" var="status" varStatus="loop">
                            <option value="${status}">${status}</option>
                        </c:forEach>                        
                    </select>
                    </div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3">Type </label>
					<div class="col-md-3">
					<select name="utype"class="form-control">
                        <c:forEach items="${UserTypes}" var="utype" varStatus="loop">
                            <option value="${utype}">${utype}</option>
                        </c:forEach>                        
                    </select>
                    </div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" required class="form-control" name="password"
								value="${newUser.password}" />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Register" />
					</div>
				</form>
</div>
</body>
</html>