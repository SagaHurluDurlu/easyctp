package com.example.demo.ControllerAPI;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.Credentials;
import com.example.demo.DTO.UserDTO;
import com.example.demo.Enum.Status;
import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
public class ControllerLogin {


	@Autowired
	UserService userService;
	
	
	
	@GetMapping("/")
	ResponseEntity<String> hello() {
	
	    return new ResponseEntity<>("Hello World 2.2 !", HttpStatus.OK);
	}
	//LOGIN
		@PostMapping("/login")
		@Procedure(MediaType.APPLICATION_JSON)
		public ResponseEntity<?> newLogin(@RequestBody Credentials user) {
			  User usr= new User();
			  usr.setUsername(user.getUsername());
			  usr.setPassword(user.getPassword());
			  if(userService.login(usr)==null)
				  return new ResponseEntity <String> ("Login fail", HttpStatus.BAD_REQUEST);
			   return new ResponseEntity < UserDTO > (userService.loginDto(usr),HttpStatus.OK);
		}
		
		//SIGN UP
		@PostMapping("/signup")
		@Procedure(MediaType.APPLICATION_JSON)
		public ResponseEntity<?> signup(@RequestBody UserDTO user) {
			 if(userService.findByEmail(user.getEmail()) != null)
				 return new ResponseEntity <String> ("Email deja exista", HttpStatus.BAD_REQUEST);
			 if(userService.findByNumberPhone(user.getNrTelefon()) != null)
				 return new ResponseEntity <String> ("Numar de telefon deja folosit", HttpStatus.BAD_REQUEST);
			User usr= new User(user.getUsername(),user.getPassword(),user.getFullName(),user.getAddress(),user.getEmail(), null, user.getNrTelefon(), null, null, null, null);
			userService.saveUser(usr, Status.SIMPLE.toString(), UserType.USER.toString(), null);
			return new ResponseEntity < UserDTO > (userService.getUserByIDDTO(usr.getId()),HttpStatus.OK);
		}
		
		// RESET PASSWORD
		@PostMapping("/reset")
		@Procedure(MediaType.APPLICATION_JSON)
		public ResponseEntity<?> reset(@RequestParam String emailParam, @RequestParam String telParam, @RequestParam String newPasswordParam, @RequestParam String oldPasswordParam) {
			 if(userService.findByEmail(emailParam) == null)
				 return new ResponseEntity <String> ("Email nu exista", HttpStatus.BAD_REQUEST);
			 if(userService.findByNumberPhone(telParam) == null)
				 return new ResponseEntity <String> ("Numar de telefon nu exista", HttpStatus.BAD_REQUEST);
			
			User user = userService.findByEmail(emailParam);
			boolean ok = BCrypt.checkpw(oldPasswordParam, user.getPassword());
			if(ok)
				{
				user.setPassword(newPasswordParam);
				userService.saveUser(user, Status.SIMPLE.toString(), UserType.USER.toString(), null);
				return new ResponseEntity <String> ("Parola schimbata cu success", HttpStatus.OK);
			
			}
			return new ResponseEntity <String> ("Parola nu e buna", HttpStatus.BAD_REQUEST);
		}
		
		@RequestMapping(value="/update", method = RequestMethod.POST)
		@Procedure(MediaType.APPLICATION_JSON)
		public ResponseEntity<?> updateDetails(@RequestBody UserDTO userDetails){
				User oldUser = userService.findByUsername(userDetails.getUsername());
				User newUser = userService.save(oldUser);
				UserDTO toReturn =  new UserDTO(newUser.getId(),newUser.getUsername(),newUser.getPassword(),newUser.getFullName(), newUser.getAdress(),newUser.getEmail(), newUser.getType());
				 return new ResponseEntity < UserDTO > (toReturn,HttpStatus.OK);
				
		}
		
		
}
