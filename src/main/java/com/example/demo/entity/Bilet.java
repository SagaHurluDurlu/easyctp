package com.example.demo.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.Enum.BiletStatus;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Bilet")
public class Bilet extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private BiletStatus status;
	private Date useDate;
	private String line;
	@ManyToOne
	protected User ownerTiket;
	
	public Bilet( BiletStatus status, Date useDate, String line) {
		super();
		this.status = status;
		this.useDate = useDate;
		this.line = line;
	}
	public BiletStatus getStatus() {
		return status;
	}
	public void setStatus(BiletStatus status) {
		this.status = status;
	}
	public Date getUseDate() {
		return useDate;
	}
	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public Bilet() {
		super();
		
	}
	
	
	

}
