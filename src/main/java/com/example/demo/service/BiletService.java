package com.example.demo.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Bilet;
import com.example.demo.repository.BiletRepository;

@Service
@Transactional
public class BiletService {
	private final BiletRepository biletRepository;

	public BiletService(BiletRepository biletRepository) {
		this.biletRepository = biletRepository;
	}
	public void save(Bilet bilet ) {
		biletRepository.save(bilet);
	}
	
	

}
