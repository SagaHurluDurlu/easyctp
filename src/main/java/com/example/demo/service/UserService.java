package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.example.demo.DTO.UserDTO;
import com.example.demo.Enum.Status;
import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
@Transactional
public class UserService {
private final UserRepository userRepository;
	
	public UserService(UserRepository userRepository) {
		this.userRepository=userRepository;
	}
	
	public void saveUser(User user,String status, String type,String cod) {
	    user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
	    user.setCod(cod);
	    user.setStatus(Status.valueOf(status));
	    user.setType(UserType.valueOf(type));;
	 	    	
		userRepository.save(user);
	}
	
	public List<User> showAllUsers(){
		List<User> users = new ArrayList<User>();
		for(User user : userRepository.findAll()) {
			users.add(user);
		}
		return users;
	}
	public List<UserDTO> showAllUsersDTO(){
		List<UserDTO> users = new ArrayList<UserDTO>();
		for(User user : userRepository.findAll()) {
			ModelMapper modelMapper = new ModelMapper();
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			users.add(userDTO);
		}
		return users;
	}
	
	
	public void deleteMyUser(Integer id) {
		userRepository.deleteById(id);
	}
	
	
	public User editUser(Integer id) {
		return userRepository.findById(id);
	}
	
	public UserDTO findByUsernameAndPasswordDTO(String username, String password) {
		User user = userRepository.findByUsernameAndPassword(username, password);
		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}
	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}
	
	public User login(User usr) {
		User userPassword=userRepository.findByUsername(usr.getUsername());
		if(userPassword==null)
			return null;
 
		boolean ok=BCrypt.checkpw(usr.getPassword(), userPassword.getPassword());
		if(ok) {
			return userPassword;
		}

			return null;
	}
	public UserDTO loginDto(User user) {

		User userPassword=userRepository.findByUsername(user.getUsername());

		boolean ok=BCrypt.checkpw(user.getPassword(), userPassword.getPassword());
		if(ok) {
			return getUserByIDDTO(userPassword.getId());
		}

			return null;
	}

	public User getUserByID(Integer id) {
		return userRepository.findById(id);
		
	}
	
	
	//DTO
	public UserDTO getUserByIDDTO(Integer id) {
		User user=userRepository.findById(id);
		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}

	public void deleteByUsername(String string) {
		userRepository.deleteByUsername(string);
	}
	
	public String generateNumber(String fullName) {
		String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		builder.append(fullName,0,2);
		int count = 5;	
		while (count-- != 0) {
		int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
		builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public User update(User oldUser, User oldUser2, String fullName, String address, String password, UserType type) {
		userRepository.save(oldUser2);
		return null;
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);

	}

	public User findByNumberPhone(String nrTelefon) {
		return userRepository.findByNrTelefon(nrTelefon);
	}


}

