package com.example.demo.ControllerAPI;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.Credentials;
import com.example.demo.DTO.UserDTO;
import com.example.demo.Enum.Status;
import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@RestController
public class ControllerProfile {


	@Autowired
    UserService userService;

    // EDIT PROFILE
    @PostMapping("/editProfile")
    @Procedure(MediaType.APPLICATION_JSON)
    public ResponseEntity<?> editProfile(@RequestBody UserDTO user, @RequestParam Integer userId) {
        User newUser = userService.getUserByID(userId);
        if(user.getAddress() != null) 
        {
            newUser.setAdress(user.getAddress());
        }

        if(user.getFullName() != null)
        {
            newUser.setFullName(user.getFullName());
        }

        if(user.getNrTelefon() != null)
        {
            newUser.setNrTelefon(user.getNrTelefon());
        }

        if(user.getEmail() != null)
        {
            newUser.setEmail(user.getEmail());
        }
        
        userService.save(newUser);
        return new ResponseEntity < String > ("Profil editat cu succes", HttpStatus.OK)
	}
}
	
	
	
