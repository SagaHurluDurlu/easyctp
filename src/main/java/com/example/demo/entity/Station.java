package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Station")
public class Station extends BaseEntity{
	@ManyToOne
	protected Traseu stations;
	private String stationName;
	
	
	public Station(Traseu stations, String stationName) {
		super();
		this.stations = stations;
		this.stationName = stationName;
	}
	
	public Traseu getStations() {
		return stations;
	}
	public void setStations(Traseu stations) {
		this.stations = stations;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

}
