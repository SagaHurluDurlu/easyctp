package security;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class FilterConfig {

  @Bean
  public FilterRegistrationBean<LoginFilter> loginFilter() {
    FilterRegistrationBean<LoginFilter> registration = new FilterRegistrationBean<>();
    registration.setFilter(new LoginFilter());
    registration.addUrlPatterns("/secured/custom/*");
    return registration;
  }
}
