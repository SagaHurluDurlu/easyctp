package com.example.demo.ControllerAPI;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Enum.AbonamentType;
import com.example.demo.entity.Abonament;
import com.example.demo.service.BiletService;
import com.example.demo.service.UserService;

@RestController
public class ControllerBilet {

	@Autowired
	UserService userService;
	@Autowired
	BiletService biletService;
	
	
	@RequestMapping(value="/buyBilet", method = RequestMethod.POST)
	@Procedure(MediaType.APPLICATION_JSON)
	public ResponseEntity<?> createAb(@RequestParam Integer id) {
		
		
		return new ResponseEntity <String> ("buy success", HttpStatus.OK);
	}
}
