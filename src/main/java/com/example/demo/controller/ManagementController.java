package com.example.demo.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.Enum.Status;
import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@Controller
public class ManagementController {

	@Autowired
	UserService userService;
	
	@GetMapping("/secured/admin/show-info")
	public String userDetails(@ModelAttribute("loguser") User user,HttpServletRequest request,Model model) {
		User loggedUser = (User)request.getSession().getAttribute("loguser");
		request.setAttribute("user", loggedUser);
		return "/secured/admin/show-info.jsp";
	}
	
	@RequestMapping("/secured/admin/register")
	public String registration(HttpSession session,Model model) {
		model.addAttribute("UserTypes",Arrays.asList(UserType.values()));
		model.addAttribute("Status",Arrays.asList(Status.values()));
		return "/secured/admin/register.jsp";
	}

	@PostMapping("/secured/admin/save-user")
	public String save(@ModelAttribute User user,@RequestParam(required=false,name="utype") String utype,@RequestParam(required=false,name="status") String status, BindingResult bindingResult,HttpSession session,HttpServletRequest request)
	{
		if (bindingResult.hasErrors()) {
            return "error";
        }
		String cod = userService.generateNumber(user.getFullName());
		userService.saveUser(user,status,utype,cod);
		return "redirect:adminHome.jsp";
	}
	
	@GetMapping("/secured/admin/show-user")
	public String showAllUsers(HttpServletRequest request) {
		request.setAttribute("users", userService.showAllUsers());
		return "/secured/admin/show-user.jsp";
	}

	@RequestMapping("/secured/admin/delete-user")
	public String deleteUser(@RequestParam int id,HttpServletRequest request) {
		userService.deleteMyUser(id);
		request.setAttribute("users", userService.showAllUsers());
		return "redirect:adminHome.jsp";
	}
	
	@RequestMapping("/secured/admin/edit-user")
	public String editUser(@RequestParam int id,HttpServletRequest request,Model model) {
		model.addAttribute("UserTypes",Arrays.asList(UserType.values()));
		model.addAttribute("Status",Arrays.asList(Status.values()));
		request.setAttribute("user", userService.editUser(id));
		return "/secured/admin/edit-user.jsp";
	}
}
