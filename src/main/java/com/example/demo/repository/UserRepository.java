package com.example.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	public User findByUsernameAndPassword(String username, String password);

	public void deleteById(Integer id);

	public User findById(Integer id);

	public User findByUsername(String username);

	public void deleteByUsername(String string);

	public User findByEmail(String email);

	public User findByNrTelefon(String nrTelefon);

}