package com.example.demo.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Traseu")
public class Traseu extends BaseEntity{

	@OneToMany(targetEntity=Station.class, mappedBy="stations", fetch=FetchType.EAGER)
	protected List<Station> listStatie;
	
	
	public Traseu(Station firstStation, Station lastStation, List<Station> listStatie) {
		super();

		this.listStatie = listStatie;
	}

	public List<Station> getListStatie() {
		return listStatie;
	}
	public void setListStatie(List<Station> listStatie) {
		this.listStatie = listStatie;
	}
	
	
}
