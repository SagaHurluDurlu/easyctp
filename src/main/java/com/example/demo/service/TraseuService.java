package com.example.demo.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Traseu;
import com.example.demo.repository.TraseuRepository;

@Service
@Transactional
public class TraseuService {

	private final TraseuRepository traseuRepository;

	public TraseuService(TraseuRepository traseuRepository) {
		this.traseuRepository = traseuRepository;
	}
	public void save(Traseu traseu ) {
		traseuRepository.save(traseu);
	}
}
