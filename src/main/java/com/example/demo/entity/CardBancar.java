package com.example.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="CardBancar")
public class CardBancar extends BaseEntity{
	private String numberCard;
	private String numberCVV;
	private Date expDate;
	@ManyToOne
	protected User ownerCard;
	@OneToMany(targetEntity=Payment.class, mappedBy="cardBancar", fetch=FetchType.EAGER)
	protected List<Payment> payments;
	
	
	
	public CardBancar() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CardBancar(String numberCard, String numberCVV, Date expDate, User ownerCard, List<Payment> payments) {
		super();
		this.numberCard = numberCard;
		this.numberCVV = numberCVV;
		this.expDate = expDate;
		this.ownerCard = ownerCard;
		this.payments = payments;
	}
	public String getNumberCard() {
		return numberCard;
	}
	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}
	public String getNumberCVV() {
		return numberCVV;
	}
	public void setNumberCVV(String numberCVV) {
		this.numberCVV = numberCVV;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public User getOwnerCard() {
		return ownerCard;
	}
	public void setOwnerCard(User ownerCard) {
		this.ownerCard = ownerCard;
	}
	public List<Payment> getPayments() {
		return payments;
	}
	public void setPayments(List<Payment> paymentList) {
		this.payments = paymentList;
	}
	
	

}
