package com.example.demo.entity;

import java.text.DateFormat;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="Audit")
public class Audit extends BaseEntity{
	private String ActionTipe;
	private String descriere;
	private DateFormat date;

	public Audit(String actionTipe, String descriere, DateFormat date) {
		super();
		ActionTipe = actionTipe;
		this.descriere = descriere;
		this.date = date;
	}
	public String getActionTipe() {
		return ActionTipe;
	}
	public void setActionTipe(String actionTipe) {
		ActionTipe = actionTipe;
	}
	public String getDescriere() {
		return descriere;
	}
	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}
	public DateFormat getDate() {
		return date;
	}
	public void setDate(DateFormat date) {
		this.date = date;
	}
	
	
}
