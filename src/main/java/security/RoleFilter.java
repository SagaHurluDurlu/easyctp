package security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.demo.Enum.UserType;
import com.example.demo.entity.User;



public class RoleFilter implements Filter {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(LoginFilter.class);

  public static final String LOGIN_PAGE = "/login";//maparea din controller, pt pagina de welcome pt user normal

  @Override
  public void doFilter(ServletRequest servletRequest,
      ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {

	  		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
	  		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

	  			// managed bean name is exactly the session attribute name
	  		User userManager = (User) httpServletRequest.getSession().getAttribute("loguser");

	  			if (UserType.ADMIN.equals(userManager.getType())) 
	  			{
	  					filterChain.doFilter(servletRequest, servletResponse);
	  			}
	  			else {
	  					LOGGER.debug("user not found");
	  					// back to login
	  					httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + LOGIN_PAGE);
	  				}
  		}

  @Override
  public void init(FilterConfig arg0) throws ServletException {
    LOGGER.debug("loginFilter initialized");
  }

  @Override
  public void destroy() {}
}