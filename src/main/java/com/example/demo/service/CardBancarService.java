package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.demo.entity.CardBancar;
import com.example.demo.repository.CardBancarRepository;


@Service
@Transactional
public class CardBancarService {
	private final CardBancarRepository cardRepository;

	public CardBancarService(CardBancarRepository cardRepository) {
		this.cardRepository = cardRepository;
	}
	public void save(CardBancar card ) {
		cardRepository.save(card);
	}
	
	

}
