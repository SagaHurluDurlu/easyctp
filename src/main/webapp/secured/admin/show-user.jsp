<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="adminHome.jsp"></jsp:include>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all users</title>

</head>
<body>
<div class="container text-center" id="tasksDiv">
				<h3>All Users</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Full Name</th>
								<th>Address</th>
								<th>Email</th>
								<th>UserName</th>
								<th>Numar telefon</th>
								<th>Status</th>
								<th>Type</th>								
								<th>Code</th>
								<th>Delete</th>
								<th>Edit</th>
								
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${users}">
							<c:url var="userLink" value="/secured/admin/show-accountByUser">
							<c:param name="userId" value="${user.id}"/>
							</c:url>
								<tr>
									<td>${user.fullName}</td>
									<td>${user.adress}</td>
									<td>${user.email}</td>
									<td>${user.username}</td>
									<td>${user.nrTelefon}</td>
									<td>${user.status}</td>
									<td>${user.type}</td>
									<td>${user.cod}</td>
									
									<td><a href="/secured/admin/delete-user?id=${user.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/secured/admin/edit-user?id=${user.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
</body>
</html>