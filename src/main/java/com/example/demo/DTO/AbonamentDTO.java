package com.example.demo.DTO;

import java.util.Date;

import com.example.demo.Enum.AbonamentType;
import com.example.demo.mappedClass.BaseEntity;

public class AbonamentDTO extends BaseEntity{

	private AbonamentType type;
	private Long price;
	private String duration;
	private Date start_date;
	private Date stop_date;
	private int numberOfTravelers;
	public AbonamentDTO(AbonamentType type, Long price, String duration, Date start_date, Date stop_date,int numberOfTravelers) {
		super();
		this.type = type;
		this.price = price;
		this.duration = duration;
		this.start_date = start_date;
		this.stop_date = stop_date;
		this.numberOfTravelers = numberOfTravelers;
	}
	public AbonamentDTO(AbonamentDTO ab) {
		super();
		this.type = ab.getType();
		this.price = ab.getPrice();
		this.duration = ab.getDuration();
		this.start_date = ab.getStart_date();
		this.stop_date = ab.getStop_date();
		this.numberOfTravelers = ab.getNumberOfTravelers();
	}
	public AbonamentType getType() {
		return type;
	}
	public void setType(AbonamentType type) {
		this.type = type;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getStop_date() {
		return stop_date;
	}
	public void setStop_date(Date stop_date) {
		this.stop_date = stop_date;
	}
	public int getNumberOfTravelers() {
		return numberOfTravelers;
	}
	public void setNumberOfTravelers(int numberOfTravelers) {
		this.numberOfTravelers = numberOfTravelers;
	}
	
	
}
