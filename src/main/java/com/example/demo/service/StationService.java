package com.example.demo.service;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Station;
import com.example.demo.repository.StationRepository;

@Service
@Transactional
public class StationService {
	private final StationRepository stationRepository;

	public StationService(StationRepository stationRepository) {
		this.stationRepository = stationRepository;
	}
	public void save(Station station ) {
		stationRepository.save(station);
	}
	

}
