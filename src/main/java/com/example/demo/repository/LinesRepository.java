package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Lines;

@Repository
public interface LinesRepository extends JpaRepository<Lines,Long>{

	public void deleteById(Integer id);

	public Lines findById(Integer id);
}
