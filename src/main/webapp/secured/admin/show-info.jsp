<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="adminHome.jsp"></jsp:include>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all users</title>

</head>
<body>
<div class="container text-center" id="tasksDiv">
				<h3>My details</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>UserName</th>
								<td>${user.username}</td>
								</tr>
						</thead>
						<thead>
							<tr>
								<th>Full Name</th>
								<td>${user.fullName}</td>
								</tr>
						</thead>
						<thead>
							<tr>
								<th>Address</th>
								<td>${user.adress}</td>
								</tr>
						</thead>
						<thead>
							<tr>
								<th>Email</th>
								<td>${user.email}</td>

							</tr>
						</thead>
					</table>
				</div>
			</div>
</body>
</html>