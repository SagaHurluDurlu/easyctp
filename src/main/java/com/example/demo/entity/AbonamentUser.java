package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.example.demo.entity.User;
import com.example.demo.mappedClass.BaseEntity;

@Entity
@Table(name="AbonamentUser")
public class AbonamentUser extends BaseEntity{
	
	@ManyToOne
	protected User owner;
	@ManyToOne
	private Abonament abonament;


	
	 public AbonamentUser(User owner, Abonament abonament) {
		super();
		this.owner = owner;
		this.abonament = abonament;

	}


	public AbonamentUser(AbonamentUser abUser) {
		 this.abonament = abUser.getAbonament();

		 }
	 
	 
	 public AbonamentUser() {
	}


	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Abonament getAbonament() {
		return abonament;
	}
	public void setAbonament(Abonament abonament) {
		this.abonament = abonament;
	}
}
