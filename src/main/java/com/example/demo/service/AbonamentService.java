package com.example.demo.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Enum.AbonamentType;
//import com.example.demo.Enum.LineType;
import com.example.demo.entity.Abonament;
//import com.example.demo.entity.Lines;
import com.example.demo.repository.AbonamentRepository;


@Service
@Transactional
public class AbonamentService {
	private final AbonamentRepository abonamentRepository;
	private  AbonamentUserService abUserService;
	private  UserService userService;

	@Autowired
	LinesService linesService;
	
	public AbonamentService(AbonamentRepository abonamentRepository) {
		this.abonamentRepository = abonamentRepository;
	}
	public void save(Abonament abonament ) {
		abonamentRepository.save(abonament);
	}
	
	public void createAbonament(Abonament abonament, Integer idOwner,String ltype1,String ltype2, String abType, Date start_date) {
		//abUser.getOwner().setId(idOwner);
		
			if(AbonamentType.valueOf(abType).equals(AbonamentType.STUDENT_BUGET))
			{
			abonament.setType(AbonamentType.STUDENT_BUGET);
			//price
			abonament.setPrice((long) 0.00);
			//date
			abonament.setStart_date(start_date);
			Date stop_date = new Date();
			if(start_date.getMonth() + abonament.getDuration()>12) {
				stop_date.setMonth(start_date.getMonth() + abonament.getDuration() -12);
				stop_date.setYear(abonament.getStart_date().getYear() + 1);	
				}
			else {
				stop_date.setMonth(start_date.getMonth() + abonament.getDuration());
				}
			abonament.setStop_date(stop_date);

			
			
			if(AbonamentType.valueOf(abType).equals(AbonamentType.STUDENT_TAXA))
			{
				abonament.setType(AbonamentType.STUDENT_TAXA);
				//price
				abonament.setPrice((long) 35.00);
				
				//date
				abonament.setStart_date(start_date);
				if(start_date.getMonth() + abonament.getDuration()>12) {
					stop_date.setMonth(start_date.getMonth() + abonament.getDuration() -12);
					stop_date.setYear(abonament.getStart_date().getYear() + 1);	
					}
				else {
					stop_date.setMonth(start_date.getMonth() + abonament.getDuration());
					}
				

			}
			else 
				if(AbonamentType.valueOf(abType).equals(AbonamentType.TWO_LINES))
				{
					abonament.setType(AbonamentType.TWO_LINES);
					//price
					abonament.setPrice((long) 95.00);
					
					//date
					abonament.setStart_date(start_date);
					if(start_date.getMonth() + abonament.getDuration()>12) {
						stop_date.setMonth(start_date.getMonth() + abonament.getDuration() -12);
						stop_date.setYear(abonament.getStart_date().getYear() + 1);	
						}
					else {
						stop_date.setMonth(start_date.getMonth() + abonament.getDuration());
						}
					abonament.setStop_date(stop_date);

					
					}
				else 
					if(AbonamentType.valueOf(abType).equals(AbonamentType.ONE_LINE))
					{
						abonament.setType(AbonamentType.ONE_LINE);
						//price
						abonament.setPrice((long) 70.00);
						
						//date
						abonament.setStart_date(start_date);
						if(start_date.getMonth() + abonament.getDuration()>12) {
							stop_date.setMonth(start_date.getMonth() + abonament.getDuration() -12);
							stop_date.setYear(abonament.getStart_date().getYear() + 1);	
							}
						else {
							stop_date.setMonth(start_date.getMonth() + abonament.getDuration());
							}
						abonament.setStop_date(stop_date);
						
						}
					else
						if(AbonamentType.valueOf(abType).equals(AbonamentType.NUMBER_60_C))
						{
							abonament.setType(AbonamentType.NUMBER_60_C);
							abonament.setPrice((long) 70.00);
							abonament.setNumberOfTravelers(60);
						}
						else
							if(AbonamentType.valueOf(abType).equals(AbonamentType.NUMBER_120_C))
							{
								abonament.setType(AbonamentType.NUMBER_120_C);
								abonament.setPrice((long) 90.00);
								abonament.setNumberOfTravelers(120);
							}
		abonamentRepository.save(abonament);
/*	//	abUserService.save(new AbonamentUser(userService.getUserByID(idOwner),abonament));
		
		// lines
		if(ltype1 != null && ltype2 != null )
		{
			linesService.save(abonament.getId(),LineType.valueOf(ltype1));
			linesService.save(abonament.getId(),LineType.valueOf(ltype2));
		}
		*/
		// stop date
		abonament.setStop_date(stop_date);
		
		//abUser.getAbonament().setId(abonament.getId());
	}
	
	}
	}
