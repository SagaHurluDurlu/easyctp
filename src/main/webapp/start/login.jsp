<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Login</title>
 <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
</head>

<div class="center">
	<form:form action="autentify" modelAttribute="user" class="form-horizontal">
	
	<div class="form-group">
	<table align="center" width="80%">
	<tr>
	<td><b>Username:</b></td>
	<td><form:input path="username" /></td>
	</tr>
	
	<tr>
	<td><b>Password:</b></td>
	<td><form:input type="password" path="password" /></td>
	</tr>

<tr>
<td></td>
<td><input type="submit" value="Login" /></td>
</tr>
</table>
</div>
	</form:form>
	
 </div>
 <div class="error">
 <p>${errorMessage}</p>
 
 </div>
	
	
</body>
</html>