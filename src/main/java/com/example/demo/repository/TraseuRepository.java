package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Traseu;

@Repository
public interface TraseuRepository extends JpaRepository<Traseu,Long>{

	public void deleteById(Integer id);

	public Traseu findById(Integer id);
}
