package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.demo.entity.AbonamentUser;
import com.example.demo.repository.AbonamentUserRepository;

@Service
@Transactional
public class AbonamentUserService {
	private final AbonamentUserRepository abonament_userRepository;

	public AbonamentUserService(AbonamentUserRepository abonament_userRepository) {
		this.abonament_userRepository = abonament_userRepository;
	}
	public void save(AbonamentUser abonamentuser) {
		abonament_userRepository.save(abonamentuser);
		
	}
	public AbonamentUser findById(Integer id) {
		return abonament_userRepository.findById(id);
		}
	

}
